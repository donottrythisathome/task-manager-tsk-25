package com.ushakov.tm.api.service;

import com.ushakov.tm.api.IService;
import com.ushakov.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IService<E> {

    void clear(@Nullable String userId);

    @Nullable
    List<E> findAll(@Nullable String userId);

    @Nullable
    List<E> findAll(@Nullable String userId, @Nullable Comparator<E> comparator);

    @NotNull
    E findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    E findOneByIndex(@Nullable String userId, @Nullable final Integer index);

    @Nullable
    E removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    E removeOneByIndex(@Nullable String userId, @Nullable Integer index);

}
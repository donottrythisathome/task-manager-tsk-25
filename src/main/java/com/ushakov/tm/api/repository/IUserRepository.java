package com.ushakov.tm.api.repository;

import com.ushakov.tm.api.IRepository;
import com.ushakov.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findOneByEmail(@NotNull String email);

    @Nullable
    User findOneByLogin(@NotNull String login);

    @Nullable
    User removeOneByLogin(@NotNull String login);

}

package com.ushakov.tm.exception.entity;

import com.ushakov.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

public class UserAlreadyExistsException extends AbstractException {

    public UserAlreadyExistsException(@NotNull final String value) {
        super("Error! User with same " + value + " already exists!");
    }

}

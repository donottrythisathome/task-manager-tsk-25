package com.ushakov.tm.exception.entity;

import com.ushakov.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User was not found!");
    }

}

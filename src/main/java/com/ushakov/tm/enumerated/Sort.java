package com.ushakov.tm.enumerated;

import com.ushakov.tm.comparator.CreatedComparator;
import com.ushakov.tm.comparator.NameComparator;
import com.ushakov.tm.comparator.StartDateComparator;
import com.ushakov.tm.comparator.StatusComparator;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", CreatedComparator.getInstance()),
    START_DATE("Sort by start date", StartDateComparator.getInstance()),
    NAME("Sort by name", NameComparator.getInstance()),
    STATUS("Sort by status", StatusComparator.getInstance());

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}

package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Update task by id.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().findOneById(userId, taskId);
        System.out.println("ENTER NAME");
        @NotNull final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @NotNull final String taskDescription = TerminalUtil.nextLine();
        serviceLocator.getTaskService().updateById(userId, taskId, taskName, taskDescription);
    }

    @Override
    @NotNull
    public String name() {
        return "update-task-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}

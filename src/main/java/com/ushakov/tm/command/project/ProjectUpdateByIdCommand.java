package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER PROJECT ID");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final Project project = serviceLocator.getProjectService().findOneById(userId, projectId);
        System.out.println("ENTER NAME");
        @NotNull final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        @NotNull final String projectDescription = TerminalUtil.nextLine();
        serviceLocator.getProjectService().updateById(userId, projectId, projectName, projectDescription);
    }

    @Override
    @NotNull
    public String name() {
        return "update-project-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}

package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER PROJECT NAME");
        @NotNull final String projectName = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().removeOneByName(userId, projectName);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    @NotNull
    public String name() {
        return "remove-project-by-name";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}

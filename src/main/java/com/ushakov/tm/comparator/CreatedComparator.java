package com.ushakov.tm.comparator;

import com.ushakov.tm.api.model.IHasCreated;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

@NoArgsConstructor
public final class CreatedComparator implements Comparator<IHasCreated> {

    @NotNull
    private static final CreatedComparator INSTANCE = new CreatedComparator();

    @NotNull
    public static CreatedComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasCreated o1, @Nullable final IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
